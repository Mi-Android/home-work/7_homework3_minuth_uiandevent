package inc.minuth.uicomponenteventhandling.model;

import java.io.Serializable;

import inc.minuth.uicomponenteventhandling.MainActivity;

public class Category implements Serializable{

    private String title;
    private int Icon;

    public Category(String title, int icon) {
        this.title = title;
        Icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public int getIcon() {
        return Icon;
    }
}
