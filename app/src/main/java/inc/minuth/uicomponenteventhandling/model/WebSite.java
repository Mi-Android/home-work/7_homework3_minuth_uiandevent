package inc.minuth.uicomponenteventhandling.model;

import android.net.Uri;

import java.io.Serializable;

public class WebSite implements Serializable{
    private String title;
    private TypeOfSite typeOfSite;
    private String address;
    private String email;
    private Category category;
    private String imagePath="";

    public WebSite(){}
    public WebSite(String title, TypeOfSite typeOfSite, String address, String email, Category category, String imagePath) {
        this.title = title;
        this.typeOfSite = typeOfSite;
        this.address = address;
        this.email = email;
        this.category = category;
        this.imagePath = imagePath;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTypeOfSite(TypeOfSite typeOfSite) {
        this.typeOfSite = typeOfSite;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getTitle() {
        return title;
    }

    public TypeOfSite getTypeOfSite() {
        return typeOfSite;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public Category getCategory() {
        return category;
    }

    public String getImagePath() {
        return imagePath;
    }
}
