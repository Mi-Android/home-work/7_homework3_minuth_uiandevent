package inc.minuth.uicomponenteventhandling.customadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import inc.minuth.uicomponenteventhandling.R;
import inc.minuth.uicomponenteventhandling.model.Category;

public class CustomSpinnerAdapter extends BaseAdapter {

    private List<Category> categories;
    private LayoutInflater inflater;

    public CustomSpinnerAdapter(Context context, List<Category> categories)
    {
        this.categories=categories;
        inflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Category getItem(int position) {
        return categories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView=inflater.inflate(R.layout.custom_spinner,null);
        TextView textView=convertView.findViewById(R.id.tvTitle);
        ImageView imageView=convertView.findViewById(R.id.imgIcon);
        textView.setText(getItem(position).getTitle());
        imageView.setImageResource(getItem(position).getIcon());
        return convertView;
    }
}
