package inc.minuth.uicomponenteventhandling.customadapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.List;

import inc.minuth.uicomponenteventhandling.R;
import inc.minuth.uicomponenteventhandling.model.TypeOfSite;
import inc.minuth.uicomponenteventhandling.model.WebSite;

public class ListDataAdapter extends RecyclerView.Adapter<ListDataAdapter.ListDataHolder>{

    private List<WebSite> webSiteList;
    private LayoutInflater inflater;
    Context context;

    public ListDataAdapter(Context context,List<WebSite>webSitesList)
    {
        inflater=LayoutInflater.from(context);
        this.webSiteList=webSitesList;
        this.context=context;
    }

    @NonNull
    @Override
    public ListDataHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=inflater.inflate(R.layout.list_data, viewGroup, false);
        return new ListDataHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListDataHolder view, int i) {
        WebSite webSite=webSiteList.get(i);
        view.tvTitle.setText(webSite.getTitle());
        view.tvEmail.setText(webSite.getEmail());
        view.tvAddress.setText(webSite.getAddress());
        view.imgCategory.setImageResource(webSite.getCategory().getIcon());
        view.tvCategory.setText(webSite.getCategory().getTitle());
        if(webSite.getTypeOfSite()== TypeOfSite.FACEBOOK)
        {
            view.rdgSiteType.check(R.id.rdFacebook);
        }
        else {
            view.rdgSiteType.check(R.id.rdWeb);
        }
        if(webSite.getImagePath().trim().equals(""))
        {
            view.imgLogo.setImageResource(R.mipmap.ic_launcher);
        }
        else
        {
           String uriString=webSite.getImagePath();
           view.imgLogo.setImageURI(Uri.parse(uriString));
        }
    }

    @Override
    public int getItemCount() {
        return webSiteList.size();
    }

    class ListDataHolder extends RecyclerView.ViewHolder{

        TextView tvTitle;
        TextView tvCategory;
        RadioGroup rdgSiteType;
        TextView tvAddress;
        TextView tvEmail;
        ImageView imgLogo;
        ImageView imgCategory;
        public ListDataHolder(@NonNull View itemView) {
            super(itemView);
            tvAddress=itemView.findViewById(R.id.tvAddress);
            tvEmail=itemView.findViewById(R.id.tvEmail);
            imgCategory=itemView.findViewById(R.id.imgCategory);
            tvTitle=itemView.findViewById(R.id.tvTitle);
            imgLogo=itemView.findViewById(R.id.imgLogo);
            rdgSiteType=itemView.findViewById(R.id.radioGroup);
            tvCategory=itemView.findViewById(R.id.tvCategory);
        }
    }
}
