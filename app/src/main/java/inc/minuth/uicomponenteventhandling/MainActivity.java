package inc.minuth.uicomponenteventhandling;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;

import java.io.File;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import inc.minuth.uicomponenteventhandling.customadapter.CustomSpinnerAdapter;
import inc.minuth.uicomponenteventhandling.model.Category;
import inc.minuth.uicomponenteventhandling.model.TypeOfSite;
import inc.minuth.uicomponenteventhandling.model.WebSite;

public class MainActivity extends AppCompatActivity {

    final int SELECT_PHOTO=10;
    List<WebSite> webSiteList;
    Uri imageUri;
    Category category;
    TypeOfSite typeOfSite;
    @BindView(R.id.edtTitle)
    EditText edtTitle;
    @BindView(R.id.radioGroup)
    RadioGroup rdgSiteType;
    @BindView(R.id.edtAddress)
    EditText edtAddress;
    @BindView(R.id.edtEmail)
    EditText edtEmail;
    @BindView(R.id.imgView)
    ImageView imgLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        webSiteList= new ArrayList();
        fillCategory();
        rdgSiteType.check(R.id.rdWeb);
    }

    private void fillCategory()
    {
        final List<Category>categories= new ArrayList<Category>()
        {
            {
               add(new Category("Computer",R.drawable.ic_computer_black));
               add(new Category("Money",R.drawable.ic_monetization_on_black));
               add(new Category("Movie",R.drawable.ic_mood_bad_black));
               add(new Category("Music",R.drawable.ic_music_video_black));
               add(new Category("Comedy",R.drawable.ic_mood_black_24dp));

            }
        };
        Spinner spinner=findViewById(R.id.spinner);
        CustomSpinnerAdapter adapter= new CustomSpinnerAdapter(this,categories);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                category=categories.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @OnClick(R.id.btnBrowse)
    public void onBtnBrowseClicked()
    {
        Intent photoPicker=new Intent(Intent.ACTION_PICK);
        photoPicker.setType("image/*");
        startActivityForResult(photoPicker,SELECT_PHOTO);
    }

    @OnClick(R.id.btnSave)
    public void onBtnSaveClicked()
    {
        typeOfSite=rdgSiteType.getCheckedRadioButtonId()==R.id.rdWeb?TypeOfSite.GENERAL_SITE:TypeOfSite.FACEBOOK;

        WebSite webSite=new WebSite();
        webSite.setTitle(edtTitle.getText().toString());
        webSite.setAddress(edtAddress.getText().toString());
        webSite.setCategory(category);
        webSite.setEmail(edtEmail.getText().toString());
        if(imageUri!=null)
        {
            webSite.setImagePath(imageUri.toString());
        }
        webSite.setTypeOfSite(typeOfSite);

        webSiteList.add(webSite);

        Intent intent=new Intent(this,DetailActivity.class);
        intent.putExtra("data", (Serializable) webSiteList);
        startActivity(intent);

    }
    @OnClick(R.id.btnView)
    public void onBtnViewClicked()
    {
        Intent intent=new Intent(this,DetailActivity.class);
        intent.putExtra("data", (Serializable) webSiteList);
        startActivity(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode)
        {
            case SELECT_PHOTO:
                if(resultCode== RESULT_OK){
                    imageUri=data.getData();
                    imgLogo.setImageURI(imageUri);

                }
        }
    }
}
