package inc.minuth.uicomponenteventhandling;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import inc.minuth.uicomponenteventhandling.customadapter.ListDataAdapter;
import inc.minuth.uicomponenteventhandling.model.WebSite;

public class DetailActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        RecyclerView recyclerView = findViewById(R.id.recycleView);

        Intent intent = getIntent();
        List<WebSite> webSiteList = (List<WebSite>) intent.getSerializableExtra("data");
        if(webSiteList.size()>0)
        {
            ListDataAdapter adapter=new ListDataAdapter(this,webSiteList);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        }
        else {
            setContentView(R.layout.blank_data);
        }

    }
}
